#include <climits>
#include <iostream>
using namespace std;

struct node {
    int data;
    struct node *left;
    struct node *right;
};

struct node* newNode(int data_) {
    struct node *anode = new struct node;
    anode->data = data_;
    anode->left = NULL;
    anode->right = NULL;
    return (anode);
}

void insert(struct node **rootRef, int data_) {
    if (*rootRef == NULL) {
        *rootRef = newNode(data_);
    } else {
        if (data_ <= (*rootRef)->data)
            insert(&((*rootRef)->left), data_);
        else {
            insert(&((*rootRef)->right), data_);
        }
    }
}

/*
 * -- in-order: in ascending order (for sorted list)
 left-root-right
 */
void printTreeInOrder(struct node *root) {
    if (root == NULL)
        return;
    printTreeInOrder(root->left);
    cout << root->data << endl;
    printTreeInOrder(root->right);

}

struct node* minValueNode(struct node *root) {
    struct node *anode = root;
    while (anode->left != NULL) {
        anode = anode->left;
    }

    return (anode);
}

bool lookup(struct node *root, int target) {

    if (root == NULL)
        return false;

    if (target == root->data)
        return true;

    if (target <= root->data) {
        return (lookup(root->left, target));
    } else {
        return (lookup(root->right, target));
    }

}

void kthsmallestHelper(struct node *root, int k, int &counter) {

    if (root == NULL || counter >= k)
        return;

    kthsmallestHelper(root->left, k, counter);

    counter = counter + 1;

    if (k == counter) {
        cout << k << "th smallest value: " << root->data << endl;
        return;
    }

    kthsmallestHelper(root->right, k, counter);

}

void kthsmallest(struct node *root, int k) {

    int counter = 0;
    kthsmallestHelper(root, k, counter);
}

/**
 * @brief Finds the total number of nodes in a BST.
 *
 * @param root      Root node of a BST
 * @param nodeCount Pointer to the number of nodes
 * @return
 */
int totalNumberOfNodes(struct node *root, int *nodeCount = NULL) {
    int count = 0;
    if (nodeCount == NULL) {
        nodeCount = &count;
    }

    // Traverse the BST in-order
    if (root != NULL) {
        totalNumberOfNodes(root->left, nodeCount);
        (*nodeCount)++;
        totalNumberOfNodes(root->right, nodeCount);
    }

    return (*nodeCount);
}

/**
 * @brief Finds the height of a BST.
 *
 * @param root    Root node of a BST
 * @param height  Pointer to the height of BST
 * @return
 */
int heightOfTree(struct node *root, int *height = NULL) {
    int count = 0;
    int leftHeight = 0;
    int rightHeight = 0;
    if (height == NULL) {
        height = &count;
    } else {
        count = *height;
    }

    // Traverse the BST pre-order
    if (root != NULL) {
        (*height)++;
        heightOfTree(root->left, &leftHeight);
        heightOfTree(root->right, &rightHeight);

        // Add the height of the child with greater value
        if (leftHeight > rightHeight) {
            (*height) += leftHeight;
        } else {
            (*height) += rightHeight;
        }
    }

    return (*height);
}

/**
 * @brief Prints a pre order traversal of a BST.
 *
 * @param root Root node of a BST
 */
void printTreePreOrder(struct node *root) {
    if (root == NULL)
        return;
    cout << root->data << endl;
    printTreePreOrder(root->left);
    printTreePreOrder(root->right);

}

/**
 * @brief Prints a post order traversal of a BST.
 *
 * @param root Root node of a BST
 */
void printTreePostOrder(struct node *root) {
    if (root == NULL)
        return;
    printTreePostOrder(root->left);
    printTreePostOrder(root->right);
    cout << root->data << endl;
}

/**
 * @brief Takes an integer target value as a parameter and
 *      deletes the node.
 *
 * @param root  Root node of a BST
 * @param data  Integer target value
 * @return Pointer to the root of the balanced BST
 */
struct node* deleteNode(struct node *root, int data) {
    if (root == NULL) {
        return NULL;
    }

    // If found the node then, remove it and balance the BST
    if (data == root->data) {
        if (root->left == NULL && root->right == NULL) {
            delete (root);
            return NULL;
        } else if (root->left == NULL) {
            struct node *temp = root->right;
            delete (root);
            return temp;
        } else if (root->right == NULL) {
            struct node *temp = root->left;
            delete (root);
            return temp;
        }

        struct node *temp = minValueNode(root->right);
        root->data = temp->data;
        root->right = deleteNode(root->right, temp->data);
    } else {
        // If the target value is less than current node then,
        // look for left child. Otherwise, look for right child.
        if (data < root->data) {
            root->left = deleteNode(root->left, data);
        } else {
            root->right = deleteNode(root->right, data);
        }
    }

    return root;
}

/**
 * @brief Checks whether a given binary tree is a BST or not.
 *
 * @param root  Root node of a BST
 * @return Returns true if the given tree is a BST, false otherwise.
 */
bool isTreeBst(struct node *root) {
    bool isBst = true;

    // Traverse the tree post-order and check if the value of the left
    // child is less than or equal to the current node and the value
    // of the right child is greater than the value of the current node.
    if (root != NULL) {
        if ((root->left != NULL) && (root->left->data > root->data)) {
            isBst = false;
        } else if ((root->right != NULL) && (root->right->data <= root->data)) {
            isBst = false;
        }

        if (isBst) {
            isBst = isTreeBst(root->left) && isTreeBst(root->right);
        }
    }

    return isBst;
}

/**
 * @brief Searches the given value and returns the address of
 *      the node if it is found, otherwise returns NULL
 *
 * @param root  Root node of a BST
 * @param data  A value to be searched for.
 * @return Returns a pointer to the requested node if found,
 *      NULL otherwise.
 */
struct node* search(struct node *root, int data) {
    if (root == NULL) {
        return NULL;
    }

    struct node *temp = NULL;
    if (root->data == data) {
        return root;
    } else if ((temp = search(root->left, data)) != NULL) {
        return temp;
    } else {
        temp = search(root->right, data);
    }

    return temp;
}

/**
 * @brief Finds the maximum element in a BST.
 *
 * @param root  Root node of a BST
 * @return Returns a pointer to the max value node.
 */
struct node* maxValueNode(struct node *root) {
    struct node *anode = root;
    while (anode->right != NULL) {
        anode = anode->right;
    }

    return (anode);
}

/**
 * @brief Helper function for kthbiggest function
 *
 * @param root     Root node of a BST
 * @param k        value of k
 * @param counter  current counter
 */
void kthbiggestHelper(struct node *root, int k, int &counter) {
    if (root == NULL || counter >= k)
        return;

    kthbiggestHelper(root->right, k, counter);

    counter = counter + 1;

    if (k == counter) {
        cout << k << "th biggest value: " << root->data << endl;
        return;
    }

    kthbiggestHelper(root->left, k, counter);
}

/**
 * @brief Finds kth biggest element in a BST
 *
 * @param root  Root node of a BST
 * @param k     value of k
 */
void kthbiggest(struct node *root, int k) {
    int counter = 0;
    kthbiggestHelper(root, k, counter);
}

/**
 * @brief Finds the minimum absolute difference in a BST
 *
 * @param root        Root node of a BST
 * @param minAbsDiff  Pointer to the minimum absolute difference value
 * @return  Returns the minimum absolute difference value in the BST.
 */
int minimumAbsDiff(struct node *root, int *minAbsDiff = NULL) {
    int diff = INT_MAX; // Set to maximum possible value
    if (minAbsDiff == NULL) {
        minAbsDiff = &diff;
    }

    if (root != NULL) {
        // Check if the value of difference between left-child and parent
        // is less than minimum absolute value.
        if ((root->left != NULL)
                && ((root->data - root->left->data) < *minAbsDiff)) {
            *minAbsDiff = root->data - root->left->data;
        }

        // Check if the value of difference between right-child and parent
        // is less than minimum absolute value.
        if ((root->right != NULL)
                && ((root->right->data - root->data) < *minAbsDiff)) {
            *minAbsDiff = root->right->data - root->data;
        }
    }

    return (*minAbsDiff);
}

/**
 * @brief Deletes the nodes in a BST
 *
 * @param root Root node of a BST
 */
void deleteTree(struct node **root) {
    if (root == NULL || *root == NULL)
        return;

    deleteTree(&((*root)->left));
    deleteTree(&((*root)->right));
    delete (*root);
    (*root) = NULL;
}

int main() {

    struct node *root = NULL;

    insert(&root, 70);
    insert(&root, 60);
    insert(&root, 80);
    insert(&root, 5);
    insert(&root, 95);
    insert(&root, 20);
    insert(&root, -100);
    printTreeInOrder(root);
    cout << "Minimum value: " << minValueNode(root)->data << endl;

    cout << "Search for 85: " << lookup(root, 85) << endl;
    kthsmallest(root, 3);

    cout << "Total number of node: " << totalNumberOfNodes(root) << endl;
    cout << "Height of BST: " << heightOfTree(root) << endl;
    cout << "Printing pre-order: " << endl;

    printTreePreOrder(root);
    cout << "Printing post-order: " << endl;
    printTreePostOrder(root);

    cout << "Deleting node with data value 60" << endl;
    root = deleteNode(root, 60);
    cout << "Printing post-order: " << endl;
    printTreePostOrder(root);

    cout << "Is tree BST: " << isTreeBst(root) << endl;

    cout << "Inserting back the node with value 60" << endl;
    insert(&root, 60);
    cout << "Printing post-order: " << endl;
    printTreePostOrder(root);

    cout << "Searching for a node with value 60..." << endl;
    struct node *temp = search(root, 60);
    if (temp == NULL) {
        cout << "Could not find a node with value 60" << endl;
    } else {
        cout << "Found a node with value: " << temp->data << endl;
    }

    cout << "Searching for a node with value 90..." << endl;
    temp = search(root, 90);
    if (temp == NULL) {
        cout << "Could not find a node with value 90" << endl;
    } else {
        cout << "Found a node with value: " << temp->data << endl;
    }

    cout << "Maximum value: " << maxValueNode(root)->data << endl;
    kthbiggest(root, 3);

    cout << "Minimum absolute difference: " << minimumAbsDiff(root) << endl;

    deleteTree(&root);

    return 0;
}
